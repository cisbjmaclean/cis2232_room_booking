package assignment7.utility;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bjmaclean
 */
public class RoomUtilityTest {
    
    public RoomUtilityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

//    /**
//     * Test of createTestData method, of class RoomUtility.
//     */
//    @Test
//    public void testCreateTestData() {
//        System.out.println("createTestData");
//        RoomUtility.createTestData();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

//    /**
//     * Test of roomCount method, of class RoomUtility.
//     */
//    @Test
//    public void testRoomCount() {
//        System.out.println("roomCount");
//        ArrayList<Room> rooms = null;
//        RoomUtility.roomCount(rooms);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

//    /**
//     * Test of roomSearch method, of class RoomUtility.
//     */
//    @Test
//    public void testRoomSearch() {
//        System.out.println("roomSearch");
//        ArrayList<Room> rooms = null;
//        RoomUtility.roomSearch(rooms);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of isRightTypeOfRoom method, of class RoomUtility.
     */
    @Test
    public void testIsRightTypeOfRoom() {
        System.out.println("isRightTypeOfRoom");
        String userRequestedRoomType = "";
        String currentRoomRoomType = "";
        boolean expResult = false;
        boolean result = RoomUtility.isRightTypeOfRoom(userRequestedRoomType, currentRoomRoomType);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
  
    /**
     * Test of isRightTypeOfRoom method, of class RoomUtility.
     * Testing for BoardRoom that a BoardRoomPro would be the right type of 
     * room for someone looking for a boardroom.
     */
    @Test
    public void testIsRightTypeOfRoom2() {
        System.out.println("isRightTypeOfRoom");
        String userRequestedRoomType = "class BoardRoom";
        String currentRoomRoomType = "class BoardRoomPro";
        boolean expResult = true;
        boolean result = RoomUtility.isRightTypeOfRoom(userRequestedRoomType, currentRoomRoomType);
        assertEquals(expResult, result);
    }    
    
}
