package exception;

/**
 * Add comment here
 *
 * @author BJ MacLean
 * @since Apr 20, 2015
 */
public class RoomDoesNotExistException extends Exception{
    public RoomDoesNotExistException(String message){
        super(message);
    }
}
