package exception;

/**
 * Add comment here
 *
 * @author BJ MacLean
 * @since Apr 20, 2015
 */
public class UserEntryException extends Exception{
    public UserEntryException(String message){
        super(message);
    }
}
