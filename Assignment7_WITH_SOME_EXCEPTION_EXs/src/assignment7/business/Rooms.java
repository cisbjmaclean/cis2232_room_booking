package assignment7.business;

import assignment7.comparator.NumberOfSeatComparator;
import assignment7.comparator.RoomNumberComparator;
import exception.RoomDoesNotExistException;
import exception.UserEntryException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Add comment here
 *
 * @author BJ MacLean
 * @since Apr 17, 2015
 */
public class Rooms {

    private static final int ROOM_DOES_NOT_EXIST = -1;

    private static String roomNum;
    private static ArrayList<Room> rooms = new ArrayList();

    public static ArrayList<Room> getRooms() {
        return rooms;
    }

    /**
     * standard javadoc here...
     *
     * @param theList
     * @return
     */
    public static ArrayList<Room> findRoomsBookedByUser(ArrayList<Room> theList) {

        System.out.print("Name:");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        ArrayList<Room> filteredList = new ArrayList();

        System.out.println("Here are the rooms booked by:" + name);
        for (Room nextRoom : theList) {
            /*
             If the next room is booked by the input name then 
             add it to the filtered list.
             */
            if (nextRoom.getReservedBy().equalsIgnoreCase(name)) {
                filteredList.add(nextRoom);
                System.out.println(nextRoom);
            }
        }
        System.out.println("------------------");

        return filteredList;
    }

    /**
     * Loop through the rooms to check if the room already exists.
     *
     * @param roomNumber
     * @return the index of the room number
     */
    public static int getRoomNumberIfExists(String roomNumber) throws RoomDoesNotExistException {
        int index = -1;
        for (int i = 0; i < Rooms.getRooms().size(); i++) {
            if (Rooms.getRooms().get(i).getRoomNumber().equals(roomNumber)) {
                index = i;
            }
        }
        
        if(index <0){
            throw new RoomDoesNotExistException("RoomNumber:"+roomNumber);
        }
        return index;
    }

    /**
     * This method will allow the user to add a new room to the collection of
     * rooms.
     *
     * @throws exception.UserEntryException
     */
    public static void addRoom() throws UserEntryException{

        //***********************************************************
        //Ask which room number the user wants to add
        //***********************************************************
        Room room = null;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter room number: ");
        
//        try{      
//        int roomNumberInt = Integer.parseInt(input.nextLine());
//        }catch(NumberFormatException nfException){
//            System.out.println("exception happened");
//            throw new UserEntryException("Room number entry error");
//        }
        
        roomNum = input.nextLine();

        //***********************************************************
        //Check to see if the room already exists
        //***********************************************************
        int roomNumberIndex=-1;
        try {
            roomNumberIndex = getRoomNumberIfExists(roomNum);
        } catch (RoomDoesNotExistException ex) {
            System.out.println(ex.getMessage());
        }

        assert roomNumberIndex>=0: "Room does not exists (index="+roomNumberIndex+")";
        
        
        //If the room does not already exist.
        if (roomNumberIndex == ROOM_DOES_NOT_EXIST) {
            roomNumberIndex = Rooms.getRooms().size();
            boolean finished = false;
            do {
                System.out.print("What type of room is this?\n" + "1) Add Room\n"
                        + "2) Computer Lab\n"
                        + "3) Board Room\n"
                        + "4) Biology lab\n"
                        + "5) Fancy Board Room");
                String choice = input.nextLine();

                //***********************************************************
                //Based on the user input, create the correct type of room.  
                //***********************************************************
                switch (choice) {
                    case "1":

                        room = new Room(roomNum);
                        finished = true;
                        break;
                    case "2":
                        room = new ComputerRoom(roomNum);
                        finished = true;
                        break;
                    case "3":
                        room = new BoardRoom(roomNum);
                        finished = true;
                        break;
                    case "4":
                        room = new BiologyLab(roomNum);
                        finished = true;
                        break;
                    case "5":
                        room = new BoardRoomPro(roomNum);
                        finished = true;
                        break;
                    default:
                        System.out.println("Invalid option");

                }
            } while (!finished);

            //Set the details for the room
            //Note the correct method will be invoked based on which type of room was created above.
            room.getRoomDetailsFromUser();

            //Add the room to the collection of rooms.  Note that as long as an object 'is a' Room 
            //(all of the types of rooms above are rooms), then it can be added to the collection of 
            //rooms.
            Rooms.getRooms().add(room);

        } else {
            String choice = "";
            System.out.println("Room already exists. Do you want to continue? (Y/N)");
            choice = input.nextLine();

            //If the user wants to continue, invoke the method to change the value of attributes in 
            //the room
            if (choice.equalsIgnoreCase("y")) {
                Rooms.getRooms().get(roomNumberIndex).getRoomDetailsFromUser();
            }
        }
    }

    /**
     * This method will allow the user to reserve a room.
     */
    public static void reserveRoom() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the room number you would like to book");
        String roomNumber = input.nextLine();

        //Check to see if the room exists.
        int roomNumberIndex = -1;
        try {
            roomNumberIndex = getRoomNumberIfExists(roomNumber);
        } catch (RoomDoesNotExistException ex) {
            //do nothing
        }
        if (roomNumberIndex < 0) {
            System.out.println("This room does not exist");
        } else {
            //Put the room from the ArrayList into a local variable.
            Room room = Rooms.getRooms().get(roomNumberIndex);
            if (!room.isReserved()) {
                room.reserveThisRoom();
            } else {
                System.out.println("This room is already booked!");
            }
        }
    }

    public static void releaseRoom() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the room number you would like to release");
        String roomNumber = input.nextLine();

        //Check if the room exists.  
        int roomNumberIndex;
        try {
            roomNumberIndex = getRoomNumberIfExists(roomNumber);
        } catch (RoomDoesNotExistException ex) {
            roomNumberIndex = -1;
        }

        if (roomNumberIndex < 0) {
            System.out.println("This room does not exist");
        } else {
            //Put the room from the ArrayList into a local variable.
            Room room = Rooms.getRooms().get(roomNumberIndex);
            //If the room is reserved, allow them to release.
            if (room.isReserved()) {
                room.releaseThisRoom();
            } else {
                System.out.println("This room is not booked!");
            }
        }
    }

    public static void showRooms() {

        Scanner input = new Scanner(System.in);
        boolean finished = false;
        do {
            System.out.print("Enter the sort option:\n"
                    + "1)  Sort by number of seats ascending\n"
                    + "2)  Sort by room number ascending\n");
            String choice = input.nextLine();

            switch (choice) {
                case "1":
                    Collections.sort(Rooms.getRooms(), new NumberOfSeatComparator());
                    for (Room room : Rooms.getRooms()) {
                        System.out.println(room);
                    }
                    finished = true;
                    break;
                case "2":
                    Collections.sort(Rooms.getRooms(), new RoomNumberComparator());
                    for (Room room : Rooms.getRooms()) {
                        System.out.println(room);
                    }
                    finished = true;
                    break;
                default:
                    System.out.println("Invalid option");

            }
        } while (!finished);

    }

}
