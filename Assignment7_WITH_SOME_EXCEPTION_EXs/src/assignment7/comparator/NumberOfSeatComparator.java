package assignment7.comparator;

import assignment7.business.Room;
import java.util.Comparator;

/**
 *
 */
public class NumberOfSeatComparator implements Comparator<Room>{

    @Override
    public int compare(Room t, Room t1) {
        
        return t.getNumberOfSeats() - t1.getNumberOfSeats();
        
    }
    
//    @Override
//    public int compare(Object t, Object t1) {
//        
//        String thisSeat = thisSeatNumber.getRoomNumber();
//        String thatSeat = thatSeatNumber.getRoomNumber();
//        
//        int compareToResult = thisSeat.compareTo(thatSeat);
//        
//        return compareToResult;
//    }
    
}
