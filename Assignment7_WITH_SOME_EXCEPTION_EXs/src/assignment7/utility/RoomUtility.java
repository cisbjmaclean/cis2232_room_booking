package assignment7.utility;

import assignment7.business.BoardRoomPro;
import assignment7.business.ComputerRoom;
import assignment7.business.Room;
import assignment7.business.Rooms;
import java.util.ArrayList;
import java.util.Scanner;

public class RoomUtility {

    public static void createTestData() {

        ComputerRoom cr = new ComputerRoom("105");
        cr.setNumberOfSeats(22);
        cr.setHasSmartBoard(true);
        cr.setNumberOfComputer(22);
        Rooms.getRooms().add(cr);

        ComputerRoom cr2 = new ComputerRoom("124");
        cr2.setHasSmartBoard(true);
        cr2.setNumberOfComputer(12);
        cr2.setReserved(true);
        cr2.setReservedBy("BJ");
        cr2.setNumberOfSeats(12);
        Rooms.getRooms().add(cr2);

        ComputerRoom cr3 = new ComputerRoom("133");
        cr3.setHasSmartBoard(true);
        cr3.setNumberOfComputer(18);
        cr3.setNumberOfSeats(18);
        Rooms.getRooms().add(cr3);

        BoardRoomPro br = new BoardRoomPro("15");
        br.setHasSmartBoard(true);
        br.setHasTeleconferencing(true);
        br.setNumOfLazyBoy(2);
        br.setNumberOfSeats(10);
        Rooms.getRooms().add(br);

    }

    public static void roomCount(ArrayList<Room> rooms) {
        int numOfRoom = 0;
        int numOfComputerRoom = 0;
        int numOfBoardRoom = 0;
        int numOfBoardRoomPro = 0;
        int numOfBiologyRoom = 0;
        String largestRoom = null;
        int largestNumOfRooms = 0;
        for (int i = 0; i < rooms.size(); i++) {

            System.out.println("This room is a: " + rooms.get(i).getClass().toString());
            if (rooms.get(i).getClass().toString().equals("class Room")) {
                numOfRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class ComputerRoom")) {
                numOfComputerRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BoardRoom")) {
                numOfBoardRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BiologyLab")) {
                numOfBiologyRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BoardRoomPro")) {
                numOfBoardRoomPro += 1;
            }
            //Per requirement #1 I added the option of having multiple rooms in the biggest room.
            if (rooms.get(i).getNumberOfSeats() == largestNumOfRooms) {//Determine largest room
                largestRoom += rooms.get(i).getRoomNumber() + " and #";
            } else {
                if (rooms.get(i).getNumberOfSeats() > largestNumOfRooms) {//Determine largest room
                    largestNumOfRooms = rooms.get(i).getNumberOfSeats();
                    largestRoom = rooms.get(i).getRoomNumber();
                }
            }
        }
        //Display the results of counting 
        System.out.println("Room Count Details Report"
                + "\nRooms: " + numOfRoom
                + "\nComputer Rooms: " + numOfComputerRoom
                + "\nBiology Labs: " + numOfBiologyRoom
                + "\nBoard Rooms: " + numOfBoardRoom
                + "\nFancy Board Rooms: " + numOfBoardRoomPro
                + "\n\nLargest Room is #" + largestRoom + " with " + largestNumOfRooms + "seats");
    }

    public static void roomSearch(ArrayList<Room> rooms) {
        Scanner input = new Scanner(System.in);
        int roomTypeNum = 0;
        do {
            System.out.println("1) Room\n"
                    + "2) Computer Lab\n"
                    + "3) Board Room\n"
                    + "4) Biology lab\n"
                    + "5) Fancy Board Room");
            try {
                roomTypeNum = Integer.parseInt(input.nextLine());
            } catch (Exception e) {
                System.out.println("Invalid input 1-5 needed");
                roomTypeNum = -1;
            }
        } while (roomTypeNum < 1 || roomTypeNum > 5);
        String roomTypeString = "";

        if (roomTypeNum == 1) {
            roomTypeString = "class Room";
        }
        if (roomTypeNum == 2) {
            roomTypeString = "class ComputerRoom";
        }
        if (roomTypeNum == 3) {
            roomTypeString = "class BoardRoom";
        }
        if (roomTypeNum == 4) {
            roomTypeString = "class BiologyLab";
        }
        if (roomTypeNum == 5) {
            roomTypeString = "class BoardRoomPro";
        }

        System.out.println("How many seats do you need?");

        boolean validEntry = false;
        int numOfSeats=0;
        while (!validEntry) {
            try {
                numOfSeats = Integer.parseInt(input.nextLine());
                validEntry = true;
            }
            catch (NumberFormatException nfe) {
                System.out.println("That is not a valid int");
            } 
        }

        for (int i = 0; i < rooms.size(); i++) {

            if (isRightTypeOfRoom(roomTypeString, rooms.get(i).getClass().toString())) {//If it is the right type of room..
                if (!rooms.get(i).isReserved()) {                           //<--and the room is not reserved.. 
                    if (rooms.get(i).getNumberOfSeats() >= numOfSeats) {    //<-- and it also has the right number of seats or more.
                        System.out.println("\n\n******************************"
                                + "\nRoom Number: " + rooms.get(i).getRoomNumber()
                                + "\nNumber of Seats: " + rooms.get(i).getNumberOfSeats()
                                + "\nSmart Board: " + rooms.get(i).isHasSmartBoard());
                    }
                }
            }
        }
    }

    /**
     * need to explain this...
     *
     * @param userRequestedRoomType
     * @param currentRoomRoomType
     * @return
     */
    public static boolean isRightTypeOfRoom(String userRequestedRoomType,
            String currentRoomRoomType) {

        switch (userRequestedRoomType) {
            case "class Room":
                return true;
            case "class ComputerRoom":
                if (currentRoomRoomType.equalsIgnoreCase("class ComputerRoom")) {
                    return true;
                }
            case "class BoardRoom":
                if (currentRoomRoomType.equalsIgnoreCase("class BoardRoom")
                        || currentRoomRoomType.equalsIgnoreCase("class BoardRoomPro")) {
                    return true;
                }
            case "class BiologyLab":
                if (currentRoomRoomType.equalsIgnoreCase("class BiologyLab")) {
                    return true;
                }
            case "class BoardRoomPro":
                if (currentRoomRoomType.equalsIgnoreCase("class BoardRoomPro")) {
                    return true;
                }
            default:
                return false;
        }

    }
}
