package assignment7;

import assignment7.utility.RoomUtility;
import assignment7.business.Rooms;
import exception.UserEntryException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class Assignment7 {

    /**
     * Main method controls program and user interface.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        RoomUtility.createTestData();

        Scanner input = new Scanner(System.in);
        String menu = "*********************************\n"
                + "Choose an option:\n"
                + "1) Add Room\n"
                + "2) Reserve Room\n"
                + "3) Release Room\n"
                + "4) Show Rooms\n"
                + "5) Room Count\n"
                + "6) Room Search\n"
                + "7) Get rooms by booker\n"
                + "0) Exit";
        int selection = -1;

        while (selection != 0) {
            System.out.println(menu);
            try {
                selection = Integer.parseInt(input.nextLine());
            } catch (Exception e) {
                selection = -1;
            }
            switch (selection) {
                case 1:
            {
                try {
                    Rooms.addRoom();
                } catch (UserEntryException ex) {
                    System.out.println("User entered bad data ("+ex.getMessage()+")");
                }

            }
                    break;
                case 2:
                    Rooms.reserveRoom();
                    break;
                case 3:
                    Rooms.releaseRoom();
                    break;
                case 4:
                    Rooms.showRooms();
                    break;
                case 5:
                    RoomUtility.roomCount(Rooms.getRooms());
                    break;
                case 6:
                    RoomUtility.roomSearch(Rooms.getRooms());
                    break;
                case 7:
                    Rooms.findRoomsBookedByUser(Rooms.getRooms());
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Invalid option(1-7).");
            }
        }

    }

}
