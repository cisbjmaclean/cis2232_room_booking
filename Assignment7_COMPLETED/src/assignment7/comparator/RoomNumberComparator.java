
package assignment7.comparator;

import assignment7.business.Room;
import java.util.Comparator;

/**

 */
    public class RoomNumberComparator implements Comparator{
    
    @Override
    public int compare(Object t, Object t1) {
        Room thisRoomNumber = (Room) t;
        Room thatRoomNumber = (Room) t1;
        
        int thisRoom = Integer.parseInt(thisRoomNumber.getRoomNumber());
        int thatRoom = Integer.parseInt(thatRoomNumber.getRoomNumber());
        
        int compareToResult = thisRoom - thatRoom;
        
        return compareToResult;
    }
}