/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment7.business;

import java.util.Scanner;

/**
 *
 * @author Ryan
 * @since March 23, 2014
 *
 * This class extends the board room class by adding a few new features such as
 * a coffee maker and a lazy boy option.
 */
public class BoardRoomPro extends BoardRoom {

    private int numOfLazyBoy = 0;
    private boolean hasCoffee;

    public int getNumOfLazyBoy() {
        return numOfLazyBoy;
    }

    public void setNumOfLazyBoy(int numOfLazyBoy) {
        this.numOfLazyBoy = numOfLazyBoy;
    }

    public boolean isHasCoffee() {
        return hasCoffee;
    }

    public void setHasCoffee(boolean hasCoffee) {
        this.hasCoffee = hasCoffee;
    }

    public BoardRoomPro(String roomNumber) {
        super(roomNumber);
    }

    public String toString() {
        return super.toString() + "\nNumber of Recliners: " + numOfLazyBoy
                + "\nHas a coffee maker: " + hasCoffee;
    }

    /**
     * Get the details from the user about this class. This will invoke the
     * super method to get the base class attributes.
     */
    public void getRoomDetailsFromUser() {
        super.getRoomDetailsFromUser();
        Scanner input = new Scanner(System.in);
        System.out.println("does this room have a coffe maker? (y/n)");
        hasCoffee = input.nextLine().equalsIgnoreCase("y");
        System.out.print("Does this room have a lazy boy recliner? (y/n) ");

        System.out.println("How many Lazy Boy Recliners does it have?");
        numOfLazyBoy = input.nextInt();

    }

}
