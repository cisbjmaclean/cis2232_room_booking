/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment7.business;

import java.util.Scanner;

/**

 */
public abstract class RoomBase {
    String roomNumber;
    int numberOfSeats;
    public abstract void getRoomDetailsFromUser();
    
    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }
    public String getRoomNumber() {
        return roomNumber;
    }
    
}
