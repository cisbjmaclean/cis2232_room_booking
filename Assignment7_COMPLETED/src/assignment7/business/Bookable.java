package assignment7.business;

/**
 *

 */
public interface Bookable {
    
    public abstract void reserveThisRoom(); 
    
    public abstract void releaseThisRoom();
    
}
