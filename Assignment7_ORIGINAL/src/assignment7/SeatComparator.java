package assignment7;

import java.util.Comparator;

/**
 *
 */
public class SeatComparator implements Comparator{
    
    @Override
    public int compare(Object t, Object t1) {
        Room thisSeatNumber = (Room) t;
        Room thatSeatNumber = (Room) t1;
        
        String thisSeat = thisSeatNumber.getRoomNumber();
        String thatSeat = thatSeatNumber.getRoomNumber();
        
        int compareToResult = thisSeat.compareTo(thatSeat);
        
        return compareToResult;
    }
    
}
