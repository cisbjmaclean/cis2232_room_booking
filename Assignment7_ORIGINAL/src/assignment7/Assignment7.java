package assignment7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**

 */
public class Assignment7 {

    private static final int ROOM_DOES_NOT_EXIST = -1;
    private static ArrayList<Room> rooms = new ArrayList();
    private static String roomNum;

    /**
     * Main method controls program and user interface.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String menu = "Choose an option:\n"
                + "1) Add Room\n"
                + "2) Reserve Room\n"
                + "3) Release Room\n"
                + "4) Show Rooms\n"
                + "5) Room Count\n"
                + "6) Room Search\n"
                + "7) Exit";
        int selection = 0;


        while (selection != 7) {
            System.out.println(menu);
            selection = input.nextInt();
            input.nextLine();
            switch (selection) {
                case 1:
                    addRoom();
                    break;
                case 2:
                    reserveRoom();
                    break;
                case 3:
                    releaseRoom();
                    break;
                case 4:
                    showRooms();
                    break;
                case 5:
                    RoomUtility.roomCount(rooms);
                    break;
                case 6:
                    RoomUtility.roomSearch(rooms);
                    break;
                case 7:
                    break;
                default:
                    System.out.println("Invalid option.");
            }
        }

    }

    /**
     * Loop through the rooms to check if the room already exists.
     *
     * @param roomNumber
     * @return the index of the room number
     */
    public static int getRoomNumberIfExists(String roomNumber) {
        int index = -1;
        for (int i = 0; i < rooms.size(); i++) {
            if (rooms.get(i).getRoomNumber() == roomNumber) {
                index = i;
            }
        }
        return index;
    }

    /**
     * This method will allow the user to add a new room to the collection of rooms.
     *
     */
    public static void addRoom() {

        //***********************************************************
        //Ask which room number the user wants to add
        //***********************************************************
        Room room = null;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter room number: ");
        roomNum = input.nextLine();
        
        
        //***********************************************************
        //Check to see if the room already exists
        //***********************************************************

        int roomNumberIndex = getRoomNumberIfExists(roomNum);

        //If the room does not already exist.
        if (roomNumberIndex == ROOM_DOES_NOT_EXIST) {
            roomNumberIndex = rooms.size();
            boolean finished = false;
            do {
                System.out.print("What type of room is this?\n" + "1) Add Room\n"
                        + "2) Computer Lab\n"
                        + "3) Board Room\n"
                        + "4) Biology lab\n"
                        + "5) Fancy Board Room");
                String choice = input.nextLine();


                //***********************************************************
                //Based on the user input, create the correct type of room.  
                //***********************************************************

                switch (choice) {
                    case "1":
                        
                        room = new Room(roomNum);
                        finished = true;
                        break;
                    case "2":
                        room = new ComputerRoom(roomNum);
                        finished = true;
                        break;
                    case "3":
                        room = new BoardRoom(roomNum);
                        finished = true;
                        break;
                    case "4":
                        room = new BiologyLab(roomNum);
                        finished = true;
                        break;
                    case "5":
                        room = new BoardRoomPro(roomNum);
                        finished = true;
                        break;
                    default:
                        System.out.println("Invalid option");

                }
            } while (!finished);
            
            //Set the details for the room
            
            
            //Note the correct method will be invoked based on which type of room was created above.
            room.getRoomDetailsFromUser();
            
            //Add the room to the collection of rooms.  Note that as long as an object 'is a' Room 
            //(all of the types of rooms above are rooms), then it can be added to the collection of 
            //rooms.
            rooms.add(room);

        } else {
            String choice = "";
            System.out.println("Room already exists. Do you want to continue? (Y/N)");
            choice = input.nextLine();
            
            //If the user wants to continue, invoke the method to change the value of attributes in 
            //the room
            if (choice.equalsIgnoreCase("y")) {
                rooms.get(roomNumberIndex).getRoomDetailsFromUser();
            }
        }
    }

    
    
    /**
     * This method will allow the user to reserve a room.  
     */
    
    public static void reserveRoom() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the room number you would like to book");
        String roomNumber = input.nextLine();
        input.nextLine();
        
        //Check to see if the room exists.
        int roomNumberIndex = getRoomNumberIfExists(roomNumber);
        if (roomNumberIndex < 0) {
            System.out.println("This room does not exist");
        } else {
            //Put the room from the ArrayList into a local variable.
            Room room = rooms.get(roomNumberIndex);
            if (!room.isReserved()) {
                room.reserveThisRoom();
            } else {
                System.out.println("This room is already booked!");
            }
        }
    }

    public static void releaseRoom() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the room number you would like to release");
        String roomNumber = input.nextLine();
        input.nextLine();
        
        //Check if the room exists.  
        int roomNumberIndex = getRoomNumberIfExists(roomNumber);
        
        if (roomNumberIndex < 0) {
            System.out.println("This room does not exist");
        } else {
            //Put the room from the ArrayList into a local variable.
            Room room = rooms.get(roomNumberIndex);
            //If the room is reserved, allow them to release.
            if (room.isReserved()) {
                room.releaseThisRoom();
            } else {
                System.out.println("This room is not booked!");
            }
        }
    }

    
    public static void showRooms() {
        
        Scanner input = new Scanner(System.in);
        boolean finished = false;
        do {
                System.out.print("Enter the sort option:\n" 
                        + "1)  Sort by number of seats ascending\n" 
                        + "2)  Sort by room number ascending");
                String choice = input.nextLine();


                switch (choice) {
                    case "1":
                        Collections.sort(rooms, new SeatComparator());
                        for(Room room : rooms) {
                        System.out.println(room);}
                        finished = true;
                        break;
                    case "2":
                        Collections.sort(rooms, new RoomComparator());
                        for(Room room : rooms) {
                        System.out.println(room);}
                        finished = true;
                        break;
                    default:
                        System.out.println("Invalid option");

                }
            } while (!finished);
              
        

    }
}




