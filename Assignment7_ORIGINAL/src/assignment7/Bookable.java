package assignment7;

/**
 *

 */
public interface Bookable {
    
    public abstract void reserveThisRoom(); 
    
    public abstract void releaseThisRoom();
    
}
