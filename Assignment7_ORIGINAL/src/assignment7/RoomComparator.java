
package assignment7;

import java.util.Comparator;

/**

 */
    public class RoomComparator implements Comparator{
    
    @Override
    public int compare(Object t, Object t1) {
        Room thisRoomNumber = (Room) t;
        Room thatRoomNumber = (Room) t1;
        
        String thisRoom = thisRoomNumber.getRoomNumber();
        String thatRoom = thatRoomNumber.getRoomNumber();
        
        int compareToResult = thisRoom.compareTo(thatRoom);
        
        return compareToResult;
    }
}