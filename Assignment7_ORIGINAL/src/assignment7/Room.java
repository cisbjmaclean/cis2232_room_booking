/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment7;

import java.util.Scanner;

/**

 */
public class Room extends RoomBase implements Bookable{
    Scanner input = new Scanner(System.in);
    private String reservedBy = "";
    private boolean reserved;
    private boolean hasSmartBoard;
    
    /**
     * Get the attribute values from the user.
     */
    
    public Room(String roomNumber) {
        this.roomNumber = roomNumber;
        
    }

    //Have this method getting attributes from the RoomBase Class such as roomNumber and SeatNumber
    @Override
    public void getRoomDetailsFromUser() {
        
        System.out.print("Enter number of seats: ");
        numberOfSeats = input.nextInt();
        input.nextLine();
        System.out.print("Does this classroom have a smart board? (Y/N)");
        hasSmartBoard = input.nextLine().equalsIgnoreCase("y");
        
    }

    public boolean isHasSmartBoard() {
        return hasSmartBoard;
    }

    public void setHasSmartBoard(boolean hasSmartBoard) {
        this.hasSmartBoard = hasSmartBoard;
    }

    public String getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
    
    /**
     * Updated this method to correspond with the Bookable Class
     */
  
    @Override
    public void reserveThisRoom(){
        this.reserved = true;
        System.out.println("Enter the name of the person reserving this room: ");
        reservedBy = input.nextLine();
    }

     /**
     * Updated this method to correspond with the Bookable Class
     */

    @Override
    public void releaseThisRoom(){
        this.reserved = false;
        reservedBy = "";
        System.out.println("Room has been released\n");
        
        
    }
    
    @Override
    public String toString() {
        String output = "\n\n******************************"
                + "\nRoom Number: " + roomNumber
                + "\nNumber of Seats: " + numberOfSeats
                + "\nReserved By: " + reservedBy
                + "\nReserved: " + reserved
                + "\nSmart Board: "+ hasSmartBoard;
        return output;
    }
}
